
import random
from cards.card_game import *
from cards.card_AI import *
from cards.card_stats import *
from operator import itemgetter
from copy import copy


cardsPossible = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]


def cardScoreAuction(card) :
	score = 0
	cpt = 0
	for c in [c2 for c2 in cardsPossible if card != c2]:
		if card > c:
			score = score +1
		cpt=cpt+1

	return score, cpt 

def cardScore(card,cardsPlaced,cardsPlayed,cardsDeck,nbPlayers) :
	cards = copy(cardsPossible)
	cards = [c for c in cards if c not in cardsPlaced and c not in cardsDeck and c != card]
	#print(cardsDeck)
	score,cpt = cardScoreRec(card,nbPlayers,cardsPlayed,cards)
	#print(str(card) + " > " + str(score) + "/" + str(cpt))
	#print((score)/(cpt))
	return score, cpt 

def cardScoreRec(card,nbPlayers,cardsList,cardsRemaining):
	#print("nbCartesJouées " + str(len(cardsList)) + " nb joueurs " + str(nbPlayers))
	#print(cardsList)
	if (len(cardsList) >= 1 and card < max(cardsList)) :
		return 0, 1

	if len(cardsList) == nbPlayers-1:
		return 1, 1

	score=0
	cpt=0
	
	if (len(cardsList) <= 1):
		return cardScoreAuction(card)

	for c in cardsRemaining :
		cardsListtmp = copy(cardsList)
		cardsListtmp.append(c)
		cardslisttmp2 = [c2 for c2 in cardsRemaining if c2 != c]
		#print(len(cardsList))
		if (len(cardsList) <= nbPlayers-1):
			s,cp = cardScoreRec(card,nbPlayers,cardsListtmp,cardslisttmp2)
			score = score + s
			cpt = cpt + cp

	return score,cpt

#AI_TAF_V1_seuilMaxS_0.82_seuilMinS_0.15_seuilMinA_0.81 => 0.6105442176870748
#AI_TAF_V1_seuilMaxS_0.42_seuilMinS_0.91_seuilMinA_0.67 => 0.6155102040816327

class AITAFFactory():
	def getAI(self,num):
		if num == 1 : return  AITAFRandom()
		elif num == 2 : return AITAFV1(0.3,0.5,0.8)
		elif num == 3 : return AITAFV1(0.25,0.5,0.8)
		elif num == 4 : return AITAFV1(0.225,0.5,0.8)
		elif num == 5 : return AITAFV1(0.2,0.5,0.8)
		else : return None


class AIgenetiqueTAF():
	def score(self,statsAgg):
		return statsAgg.avg_round_won
	
	def getAI(self,listValues):
		return AITAFV1(listValues[0],listValues[1],listValues[2])

class AlgoGenetiqueTAFFactory():
	
	def getAlgoGenetique(self,nbGeneration,nbIndividus,fic):
		values = [0.3,0.5,0.8]
		params = []
		params.append(ParametreAlgoGenetique(0,1,0.1))
		params.append(ParametreAlgoGenetique(0,1,0.1))
		params.append(ParametreAlgoGenetique(0,1,0.1))
		heuristic = ElementAlgoGenetique(params,values)
		heuristic.fitScore=0.6
		algoAI = AlgoGenetique(StatsAggFactory(),AIgenetiqueTAF(),heuristic,nbGeneration,nbIndividus,fic)
		algoAI.initialisation()	
		return algoAI	

class AITAFRandom(AI):
	def __init__(self):
		AI.__init__(self,0,'AI_TAF_Random')

	def play(self,game,player):
		#print(player.name + " -> " + str(game.state) + ";" + str(len(player.cards)))
			
		if game.state == Game.SELECTION and (len(player.selected) == 0 or len(player.cards) == 1):
			if len(player.cards) > 0 :
				index = random.randrange(0,len(player.cards))
				return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index)
		
		elif game.state == Game.AUCTION :
			if len(player.cards) > 0 :
				value = random.randrange(0,len(player.cards))
				return MessageValue(Message.GAME,Deck.CHOICE,player.name,"Input",[value])

		elif game.state == Game.EXPECTATION :
			value = 22
			return MessageValue(Message.GAME,Deck.CHOICE,player.name,"Input",[value])

		elif game.state == Game.VALIDATION:
			return MessageDeck(Message.GAME,Deck.SELECTED,	player.name,player.name,0)	



class AITAFV1(AI):
	def __init__(self,seuilMax,seuilMinSelection,seuilMinAuction):
		AI.__init__(self,0,'AI_TAF_V1_seuilMaxS_' + str(seuilMax) + '_seuilMinS_' + str(seuilMinSelection) + '_seuilMinA_' + str(seuilMinAuction))
		self.seuilMax = seuilMax
		self.seuilMinAuction = seuilMinAuction
		self.seuilMinSelection = seuilMinSelection

	def play(self,game,player):
		#print(player.name + " -> nbCartes : " +  str(len(player.cards)))
			
		if game.state == Game.SELECTION and (len(player.selected) == 0 or len(player.cards) == 1):
			if len(player.cards) > 1 :
				cardsPlayed = []
				#print(player.name + " Selection")
				for pl in game.players:
					for c in pl.selected:
						cardsPlayed.append(int(c.value))
				cardsDeck = copy(player.cards)
				scores=[]
				for card in player.cards :
					cardsDeck.remove(card)
					score,cpt = cardScore(int(card.value),game.cardsPlaced,cardsPlayed,list(map(lambda x: int(x.value), cardsDeck)) , len(game.players))
					scores.append((card,score/cpt))
					#print(str(card.value)  + " -> " + str(score/cpt))
				nbAnnonce = player.auction 
				nbPlis = player.current_score

				seuilmin = self.seuilMinSelection
				seuilmax = self.seuilMax
				minimum=22
				maximum=0
				cardSelected = None
				if (nbPlis < nbAnnonce) :
					
					for c,s in scores :
						if(s >= seuilmin):
							seuilmin = s
							cardSelected=c
					if cardSelected is None :
						for c,s in scores :
							if(int(c.value) <= minimum and (int(c.value)!=0 or len(player.cards) == 1 )):
								minimum=int(c.value)
								cardSelected=c
				else :
					for c,s in scores :
						if(s <= seuilmax):
							seuilmax=s
							cardSelected=c
					if cardSelected is None :
						for c,s in scores :
							if(int(c.value) <= minimum):
								minimum=int(c.value)
								cardSelected=c
					

				index = random.randrange(0,len(player.cards))
				#print(player.name + " -> " + str(player.cards.index(cardSelected)))
				return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,player.cards.index(cardSelected))
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,0)
		
		elif game.state == Game.AUCTION :
			if len(player.cards) > 0 :
				scores=[]
				value=0
				#print(player.name + " Auction")
				for card in player.cards :
					score,cpt = cardScoreAuction(int(card.value))
					scores.append((card,score/cpt))
					
					#print(str(card.value)  + " -> " + str(score/cpt))
				nbAnnonce = player.auction 
				nbPlis = player.current_score

				seuilmin = self.seuilMinAuction
				for c,s in scores :
					if s >= seuilmin : value = value + 1
				#print(game.players[game.round_last_player])
				#print(player)
				if (game.players[game.round_last_player] == player):
					#print("Auction = " + str(game.nbCardsAuction))
					#print("Value = " + str(value))
					if (game.nbCardsAuction + value == game.nbCards ):
						if (value == 0) : value = 1
						else: value = value - 1

				return MessageValue(Message.GAME,Deck.CHOICE,player.name,"Input",[value])

		elif game.state == Game.EXPECTATION :
			nbAnnonce = player.auction 
			nbPlis = player.current_score
			if (nbPlis < nbAnnonce) :			
				value = 22
			else:
				value = 0
			return MessageValue(Message.GAME,Deck.CHOICE,player.name,"Input",[value])

		elif game.state == Game.VALIDATION:
			print(player.name + " valide")
			return MessageDeck(Message.GAME,Deck.SELECTED,	player.name,player.name,0)	
