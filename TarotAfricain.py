import pygame
import sys
import os
import datetime

sys.path.append("..")
from cards.card_game import *
from cards.card_manager import *
from cards.card_stats import *
from card_AITAF import *
from cards.card_display import *

from gameWindow.GameWindowTK import *

#Define sizes
#-----------------------
SCREEN_WIDTH=1000
SCREEN_HEIGHT=800
CARD_WIDTH = 110
CARD_HEIGHT = 190
#-----------------------


class Rule(object):
	def __init__(self, name):
		self.name = name

	def applyRule(self,card,up,left,down,right):
		return None,None,None

class GameTAF(Game):
	
	def __init__(self,general_params,game_params):
		Game.__init__(self,general_params,game_params)
		self.awaited = []
		
		self.nbCards=0
		self.nbCardsPlayed=0
		self.nbCardsAuction=0

		self.stack = []
		self.player=0
		self.round_player=0
		self.round_last_player=0
		self.rules = []
		self.rules_combo = []
		self.global_rules= []
		self.cardsPlaced = []
		#self.tab = [[None,None,None],[None,None,None],[None,None,None]]

	def refreshUiVars(self):
		Game.refreshUiVars(self)
		self.uiVars["nb_hands"] = str(self.nbCards)	

	def start(self):
		self.state = Game.INITIALISATION
		for pl in self.players:
			pl.selected = []
		self.gameInitialisation()
	
	def step(self):
		if(not self.isGameOver()):
			if self.state == Game.DISTRIBUTION:

				self.board.fillStock()
				self.board.shuffle()
				
				for i in range (0,self.nbCards):
					for pl in self.players:	
						card = self.board.draw()
						card.hidden = False
						if self.nbCards == 1 :
							if pl == self.players[0]:
								card.hidden = True
							else :
								pl.selected = [card]
						pl.addBack(card)
			
				
					
				
				#self.round_player = self.changePlayer(self.player)
				#self.awaited.append(self.players[self.round_player].name)
				self.state = Game.AUCTION
				self.changed=True
			
			elif self.state == Game.AUCTION and len(self.awaited) == 0:
					if self.round_player != -1:
						#self.nbCardsAuction = self.nbCardsAuction + self.players[self.round_player].auction
						self.round_player = self.changePlayer(self.round_player)
						self.awaited.append(self.players[self.round_player].name)
						list_auction = []
						for i in range (0,self.nbCards+1):
							if self.nbCardsAuction + i != self.nbCards or self.round_last_player != self.round_player :
								list_auction.append(i)
						value = {"title":"TAF","text":"nombre de plis","value":{"auction":list_auction}}
						self.addOutputMessage(MessageValue( Message.CHOICE,Deck.CHOICE, self.players[self.round_player].name, "output", value))
						self.players[self.round_player].tag = "-"

					else:
						#self.nbCardsAuction = self.nbCardsAuction + self.players[self.player].auction
						self.round_player = self.round_last_player
						self.state = Game.SELECTION
					
					self.changed=True


			elif self.state == Game.SELECTION and len(self.awaited) == 0:
					if self.round_player != -1:
						self.round_player = self.changePlayer(self.round_player)
						self.awaited.append(self.players[self.round_player].name)
						self.players[self.round_player].tag = " "
					else:
						self.state = Game.PRE_ACTION
						self.round_player = self.round_last_player
					self.changed=True


			elif self.state == Game.PRE_ACTION:
				for p in self.players:
					#print(p.name + " => carte selectionée :" + str(p.selected[0].value))				
					if p.selected[0].value == 0:
						print(p.name + " => carte selectionée :" + str(p.selected[0].value))
						self.awaited.append(p.name)
						list_choice = [0,22]
						value = {"title":"TAF","text":"choisir une valeur pour l'excuse","value":{"auction":list_choice}}
						self.addOutputMessage(MessageValue( Message.CHOICE,Deck.CHOICE, p.name, "output", value))
						self.state = Game.EXPECTATION
						self.changed=True
						return
				self.state = Game.ACTION


			elif self.state == Game.ACTION:
				player = None
				valmax = -1
				for p in self.players:
					if int(p.selected[0].tag) > valmax:
						player = p
						valmax = int(p.selected[0].tag)
				player.current_score = player.current_score + 1
				self.round_player = self.beforePlayer(self.players.index(player))
				self.round_last_player = self.beforePlayer(self.players.index(player))
				self.nbCardsPlayed = self.nbCardsPlayed+ 1
				self.state = Game.REFLECTION
				self.changed=True

			elif self.state == Game.REFLECTION:
				end=False
				if(self.nbCardsPlayed == self.nbCards):
					for p in self.players:
						if (p.auction == p.current_score):
							p.stats[-1].round_won = p.stats[-1].round_won + 1
						p.score = p.score - abs(p.auction - p.current_score)
						if p.score <= 0 : end =True
					if end :
						self.round_count = 0
						valmax = -7
						for p in self.players:
							p.stats[-1].game_played = 1
							p.stats[-1].score = p.score
							if p.score > valmax:
								valmax = p.score
						winners = [p for p in self.players if p.score == valmax]
						for winner in winners:							
							winner.victory = winner.victory + 1
							winner.stats[-1].game_won = 1
						self.saveStats()
						if self.nb_games  == self.game_count:
							self.state = Game.GAME_OVER
						else :
							self.game_count = self.game_count+1
							self.state = Game.VALIDATION
					else :
						self.state = Game.VALIDATION

					if self.state == Game.VALIDATION :
						#print("Nouvelle manche")
						
						for p in self.players :
							p.stats.append(Stats())
							#p.selected = []
							self.awaited.append(p.name)
				else:
					for p in self.players:
						p.selected = []
						p.tag= "-"
					self.state = Game.SELECTION
				self.changed=True
			
			
			elif self.state == Game.EXPECTATION and len(self.awaited) == 0:
				self.state = Game.ACTION

			elif self.state == Game.VALIDATION and (len(self.awaited) == 0):
				self.state = Game.INITIALISATION

			elif self.state == Game.INITIALISATION:
				self.round_count = self.round_count + 1
				loosers = len([p for p in self.players if p.score <= 0])

				for p in self.players :
					p.cards=[]
					p.tag="?"	
					p.selected=[]
					p.current_score = 0
					p.auction = 0
					if (loosers >= 1):
						p.score = self.game_params["score"]
					p.stats[-1].round_played = p.stats[-1].round_played + 1
				self.nbCards=self.nbCards-1
				if self.nbCards <= 0:
					self.nbCards = self.board.stock_init.getNbCards() // self.getNbPlayers()
					self.player = self.changePlayer(self.player)
					#self.round_count = 1
				self.round_player = self.player
				self.round_last_player = self.player
				self.players[self.player].tag = "X"
				#print(self.players[self.round_last_player].name + " va finir le tour")
				self.nbCardsPlayed=0
				self.nbCardsAuction=0
				self.cardsPlaced = []
				self.state = Game.DISTRIBUTION

	def action(self, message,player,deck,card):
		
		Game.action(self, message,player,deck,card)
		if(not  self.isGameOver()):
			if(player.name in self.awaited):
				
				if (message.messageType == Deck.CHOICE) and self.state == Game.AUCTION :
					player.auction = int(message.value[0])
					if self.round_player != self.round_last_player or self.nbCardsAuction + player.auction != self.nbCards : 
						self.nbCardsAuction = self.nbCardsAuction + self.players[self.round_player].auction
						if self.round_player== self.round_last_player : self.round_player = -1
						
						
					else:
						print("Invalid auction")
						self.round_player = self.beforePlayer(self.round_player)
					self.awaited.remove(player.name)
					

				elif (message.messageType == Deck.PLAYER) and player.name == self.players[self.round_player].name and player.name == deck.name and self.state == Game.SELECTION :
					#print("joueur " + self.players[self.round_player].name)
					card.marked = True 
					player.selected = [card]
					player.removeCard(card)
					self.cardsPlaced.append(card.value)
					card.hidden = False
					self.changed=True
					self.awaited.remove(player.name)
					if self.round_player== self.round_last_player : self.round_player = -1

				elif (message.messageType == Deck.CHOICE) and self.state == Game.EXPECTATION :
					player.selected[0].tag = int(message.value[0])
					self.awaited.remove(player.name)

				#elif(message.messageType == Deck.BOARD)  and self.state == Game.EXPECTATION:
				#	self.awaited.remove(player.name)
				#	p, card = self.stack[0]
				#	self.pickUp(p,deck)
				#	self.throwCard(p,deck, card)
				#	self.state = Game.ACTION
				#	self.changed=True

				elif(message.messageType == Deck.SELECTED)  and self.state == Game.VALIDATION:
					self.awaited.remove(player.name)
				
	def getCards(self,j,i):
		moveup=j-1
		moveleft=i-1
		movedown=j+1
		moveright=i+1
		up=None
		left=None
		down=None
		right=None

		#print("u " + str(moveup)+ " : " + str(self.i))
		#print("l " + str(self.j)+ " : " + str(moveleft))
		#print("d " + str(movedown)+ " : " + str(self.i))
		#print("r " + str(self.j)+ " : " + str(moveright))

		

		return up,left,down,right

	def changePlayer(self,player):
		return (player+1)%self.getNbPlayers()

	def beforePlayer(self,player):
		return (player-1)%self.getNbPlayers()

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value < card_value and card_checked.b_player != card.b_player :
			#print(card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			card_checked.hidden = True
			self.stack.append(card_checked)

	def loadInterfacedDecks(self):
		self.interfacedDecks.append(InterfacedDeckDescriptor(self.players[0],self.players[0].deckType))		
		self.interfacedDecks.append(InterfacedDeckDescriptor(self.players[0],Deck.SELECTED))

		for i in range (1,self.game_params["nb_players"]):
			self.interfacedDecks.append(InterfacedDeckDescriptor(self.players[i],Deck.SELECTED))

		for i in range(0,self.game_params["nb_max_players"]-len(self.interfacedDecks)+1):
			self.interfacedDecks.append(InterfacedDeckDescriptor(Deck("filler" + str(i)),Deck.SELECTED))
	
class GameFactoryTAF(GameFactory):
	
	def getGame(self):
		return GameTAF(self.general_params,self.game_params)

def main():
	
	general_params=loadParams("general_params.json")
	game_params=loadParams(general_params["game_params"])
	#cards = loadCards(general_params["cards"])
	gameSceneFactoryList=[]
	gameSceneFactoryList.append(GameMenuNbPlayersFactory("TAF"))
	gameSceneFactoryList.append(GameLogicFactory(GameManagerFactory(GameFactoryTAF(general_params,game_params),AITAFFactory(),loadCards)))

	GameWindowTK.display(general_params,game_params,gameSceneFactoryList,SimpleTransitionManager())
	

if __name__ == '__main__':
	main()
	#debug_display_cards(loadCards)
